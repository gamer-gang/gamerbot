# a cringey discrod bot

## how to run

- install postgresql and create the database `gamerbot`.
- use your favorite package manager to install dependencies.
- set `DISCORD_TOKEN` and `YT_API_KEY` in `.env`.
- run the `dev` or `prod` scripts to start the bot.

## debugging (vscode)

- run `yarn watch` in a terminal
- `debug dev` in vscode "go to file" menu or something
